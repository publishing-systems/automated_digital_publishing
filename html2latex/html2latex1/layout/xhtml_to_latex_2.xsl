<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2012-2019  Stephan Kreutzer

This file is part of automated_digital_publishing.

automated_digital_publishing is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

automated_digital_publishing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with automated_digital_publishing. If not, see <http://www.gnu.org/licenses/>.
-->
<!-- This file is based on haggai2latex10.xsl of Free Scriptures (see https://gitlab.com/free-scriptures/free-scriptures/ and http://www.free-scriptures.org). -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  <xsl:output method="text" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:text>\documentclass[8pt,oneside]{extbook}&#xA;</xsl:text>
    <xsl:text>% This file was created by xhtml_to_latex_2.xsl, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/publishing-systems/automated_digital_publishing/ and http://www.publishing-systems.org).&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\usepackage[utf8]{inputenc}&#xA;</xsl:text>
    <xsl:text>\usepackage[a6paper,left=1cm,right=1cm,top=2cm,bottom=0.25cm]{geometry}&#xA;</xsl:text>
    <!--xsl:text>\usepackage{ngerman}&#xA;</xsl:text-->
    <xsl:text>\usepackage{bigfoot}&#xA;</xsl:text>
    <xsl:text>\usepackage{endnotes}&#xA;</xsl:text>
    <xsl:text>\usepackage{etoolbox}&#xA;</xsl:text>
    <xsl:text>\usepackage{soulutf8}&#xA;</xsl:text>
    <xsl:text>\usepackage{seqsplit}&#xA;</xsl:text>
    <xsl:text>\usepackage{perpage}&#xA;</xsl:text>
    <xsl:text>\usepackage{ifthen}&#xA;</xsl:text>
    <xsl:text>\usepackage{fancyhdr}&#xA;</xsl:text>
    <xsl:text>\usepackage{amssymb}&#xA;</xsl:text>
    <xsl:text>\usepackage{graphicx}&#xA;</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\setlength{\parskip}{0pt}&#xA;</xsl:text>
    <!-- bigfoot -->
    <xsl:text>\DeclareNewFootnote[para]{default}&#xA;</xsl:text>
    <xsl:text>\expandafter\def\csname @makefnbreak\endcsname{\unskip\linebreak[0]\quad}&#xA;</xsl:text>
    <!-- perpage -->
    <xsl:text>\MakePerPage{footnotedefault}&#xA;</xsl:text>
    <!-- fancyhdr -->
    <xsl:text>\pagestyle{fancy}&#xA;</xsl:text>
    <xsl:text>\fancyhead{}&#xA;</xsl:text>
    <xsl:text>\fancyfoot{}&#xA;</xsl:text>
    <xsl:text>\fancyhead[C]{\leftmark}&#xA;</xsl:text>
    <xsl:text>\fancyhead[L]{\ifthenelse{\isodd{\value{page}}}{}{\thepage}}&#xA;</xsl:text>
    <xsl:text>\fancyhead[R]{\ifthenelse{\not\isodd{\value{page}}}{}{\thepage}}&#xA;</xsl:text>
    <xsl:text>\setlength{\headsep}{4.6pt}&#xA;</xsl:text>
    <xsl:text>\renewcommand{\headrulewidth}{0.5pt}&#xA;</xsl:text>
    <xsl:text>
\makeatletter
  \renewcommand\footnoterule{%
  \kern-3\p@
  \hrule\@width \textwidth
  \kern2.6\p@}
\makeatother
&#xA;</xsl:text>
    <xsl:text>\newcommand{\link}[2]{\ul{#1}\allowbreak\endnote{\texttt{\seqsplit{#2}}}}&#xA;</xsl:text>
    <xsl:text>\renewcommand{\seqinsert}{\ifmmode{\allowbreak}\else{\discretionary{\kern\fontdimen2\font}{\rotatebox[origin=c]{180}{$\Lsh$}}{}}\fi}&#xA;</xsl:text>
    <xsl:text>\def\enoteheading{\markboth{Links}{Links}\centerline{\large Links}}&#xA;</xsl:text>
<xsl:text>
\makeatletter
  \renewcommand*\makeenmark{\hbox{[\theenmark]}}
  \renewcommand*\notesname{Links}
  \patchcmd{\theendnotes}{\makeatletter}{\makeatletter\renewcommand\makeenmark{[\theenmark]\,}}{}{}
  \patchcmd{\enoteformat}{1.8em}{0pt}{}{}
\makeatother
&#xA;</xsl:text>
     <xsl:text>\newcounter{header1}&#xA;&#xA;</xsl:text>
    <xsl:text>\begin{document}&#xA;</xsl:text>
    <xsl:if test="./xhtml:html/xhtml:head/xhtml:title">
      <xsl:text>\begin{center}&#xA;</xsl:text>
      <xsl:text>\large </xsl:text>
      <xsl:value-of select="/xhtml:html/xhtml:head/xhtml:title//text()"/><xsl:text>&#xA;</xsl:text>
      <xsl:text>\end{center}&#xA;</xsl:text>
    </xsl:if>
    <xsl:apply-templates/>
    <xsl:text>\IfFileExists{\jobname.ent}{%&#xA;</xsl:text>
    <xsl:text>  \newpage&#xA;</xsl:text>
    <xsl:text>  \newgeometry{left=1.6cm,right=1cm,top=1.23cm,bottom=0.95cm}&#xA;</xsl:text>
    <xsl:text>  \fancyhfoffset[E,O]{0pt}&#xA;</xsl:text>
    <xsl:text>  \theendnotes&#xA;</xsl:text>
    <xsl:text>  \restoregeometry&#xA;</xsl:text>
    <xsl:text>}{}&#xA;</xsl:text>
    <xsl:text>\end{document}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:p">
    <xsl:apply-templates/>
    <!-- Default TeX paragraph. -->
    <xsl:text>&#xA;&#xA;</xsl:text>
  </xsl:template>
  <xsl:template match="/xhtml:html/xhtml:body//xhtml:p//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h1">
    <xsl:text>\stepcounter{header1}&#xA;</xsl:text>
    <xsl:text>\ifnum\value{header1}&gt;1&#xA;</xsl:text>
    <xsl:text>  \pagebreak{}&#xA;</xsl:text>
    <xsl:text>\fi&#xA;</xsl:text>
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;</xsl:text>
    <xsl:text>\markboth{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}{</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h1//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h2">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:text>\small </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h2//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h3">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:text>\footnotesize </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h3//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h4">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:text>\scriptsize </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h4//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h5">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:text>\tiny </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h5//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h6">
    <xsl:text>\begin{center}&#xA;</xsl:text>
    <xsl:text>{\fontsize{4}{6}\selectfont </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>}</xsl:text>
    <xsl:text>&#xA;</xsl:text>
    <xsl:text>\end{center}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:h6//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:ul">
    <xsl:text>\begin{itemize}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{itemize}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:ol">
    <xsl:text>\begin{enumerate}&#xA;</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>\end{enumerate}&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:li">
    <xsl:text>\item </xsl:text>
    <xsl:apply-templates/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:li//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:a">
    <xsl:choose>
      <xsl:when test="@href">
        <xsl:text>\link{</xsl:text>
        <xsl:apply-templates/>
        <xsl:text>}{</xsl:text>
        <xsl:value-of select="@href"/>
        <xsl:text>}</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="/xhtml:html/xhtml:body//xhtml:a//text()">
    <xsl:value-of select="."/>
  </xsl:template>

  <xsl:template match="text()|@*"/>

</xsl:stylesheet>
